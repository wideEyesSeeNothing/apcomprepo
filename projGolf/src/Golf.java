import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Golf
{
    //Each row stores rounds of golf for a particular player
    public int[][] scores;
    /**
     * Constructor for objects of class Golf
     */
    public Golf()
    {
        int[][] myScores= { {67, 73, 79},
                {71, 70, 68},
                {78, 76, 75}};

        scores = myScores;
    }
    /**
     * Constructor for objects of class Golf
     */
    public Golf( int [][] myScores)
    {
        scores = myScores;
    }
    /**
     * displays the scores in columnar form. Golfer # is the header for each column
     */
    void displayColumns()
    {
        System.out.println("SCORES");
        System.out.println("0\t1\t2\t3\t4\t5"); //\t will space to the next tab
        System.out.println("-\t-\t-\t-\t-\t-");
        for (int i = 0; i < scores[0].length; i++) {
            for (int j = 0; j < scores.length; j++) {
                System.out.print(scores[j][i] + " \t");
            }
            System.out.println();
        }
    }
    /**
     * returns the total score of a particular player (0 - n)
     */
    int getScore(int player)
    {
        return IntStream.range(0,scores[0].length).map(x -> scores[player][x]).sum();
    }

    /**
     * returns the index # of the winner. That is the golfer with the lowest sum of the
     rounds
     */
    int findWinner()
    {
        int[] temp = IntStream.range(0,scores[0].length).map(this::getScore).toArray();
        List<Integer> score = Arrays.stream(temp).boxed().collect(Collectors.toList());
        return IntStream.range(0,temp.length).reduce((a,b) -> score.get(a) > score.get(b) ? b:a).getAsInt();
    }
    /**
     *returns the average round of all of the golfers
     *
     */
    double averageRound()
    {
        return Arrays.stream(scores).flatMapToInt(Arrays::stream).average().getAsDouble();
    }
    public static void main(String args[])
    {
        Golf masters = new Golf();
        System.out.println("MASTERS1");
        masters.displayColumns();
        System.out.println();
        System.out.println("Player 2's Score is: " + masters.getScore(2));
        System.out.println("Player " + masters.findWinner() + " is the winner");
        System.out.println("The average score for all of the rounds is : " +
            masters.averageRound());

        System.out.println();
        System.out.println("MASTERS2");
        int[][] scores2= { {73, 72, 71,70},
                {67, 76, 67, 74},
                {72, 70, 74, 70},
                {72, 72, 72, 72},
                {71, 67, 70, 78},
                {68, 70, 72, 77}};
        Golf masters2 = new Golf(scores2);
        masters2.displayColumns();
        System.out.println();
        System.out.println("Player 3's Score is: " + masters2.getScore(3));
        System.out.println("Player " + masters2.findWinner() + " is the winner");
        System.out.println("The average score for all of the rounds is : " +
            masters2.averageRound());

    }

}