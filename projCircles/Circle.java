public class Circle {
    private double radius;
    public Circle(){
        radius = 1;
    }
    public Circle(double radius){
        this.radius = radius;
    }
    public void changeRadius(double radius){
        this.radius = radius;
    }
    @Override
  public String toString() {
    return String.format("Circle \n\tradius: %f\n\tarea: %f\n\tdiameter: %f\n\tcircumference: %f",
    radius, Math.PI*Math.pow(radius,2), radius*2, radius*2*Math.PI);
  }
}