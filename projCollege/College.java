
public class College {
	String institution,  nickname;
	int undergraduates, graduates, faculty;

	// complete this constructor


	public College(String institution, String nickname, int undergraduates, int graduates, int faculty) {
		this.institution = institution;
		this.nickname = nickname;
		this.undergraduates = undergraduates;
		this.graduates = graduates;
		this.faculty = faculty;
	}

	public String getInstitution() {
		return institution;
	}

	public String getNickname2() {
		return nickname;
	}

	public int getUndergraduates() {
		return undergraduates;
	}

	public int getGraduates() {
		return graduates;
	}

	public int getFaculty() {
		return faculty;
	}

	public int getPop(){
		return undergraduates + graduates;
	}

	public double getRatio(){return ((double)faculty/(double)(undergraduates + graduates));}

	@Override
	public String toString() {
		return "College{" +
				"institution='" + institution + '\'' +
				", nickname='" + nickname + '\'' +
				", undergraduates=" + undergraduates +
				", graduates=" + graduates +
				", faculty=" + faculty +
				'}';
	}

	// you can use this tester to make sure your toString and constructor works
	public static void main(String[] args) {
		College one = new College("Brown University","Bears",6670,3061,736);
		System.out.println(one);	}
}
