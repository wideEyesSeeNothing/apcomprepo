
import java.util.Arrays;
import java.util.stream.IntStream;

public class CollegeDataBase {
	/**
	 * CollegeDataBase contains an array of College objects
	 */
	private College[] list;

	// this constructor is already complete � no other constructors needed
	public CollegeDataBase () {
		list = new College[8];
		list[0] =  new College("Brown University","Bears",6670,3061,736);
		list[1] =  new College("Columbia University","Lions",8868,20116,3763);
		list[2] =  new College("Cornell University","Big Red",15182,8418,2908);
		list[3] =  new College("Dartmouth College","Big Green",4310,2099,571);
		list[4] =  new College("Harvard University","Crimson",6699,13120,4671);
		list[5] =  new College("University of Pennsylvania","Quakers", 10496,11013,4464);
		list[6] =  new College("Princeton University","Tigers",5394,2879,1172);
		list[7] =  new College("Yale University","Bulldogs",5453,6859,4140);	}

	/**
	 * Method prints all colleges names followed by their nicknames
	 */
	public void printSchoolsNicknames() {
		Arrays.stream(list).map(c -> c.getInstitution() + " : " + c.getNickname2()).forEach(System.out::println);
	}

	/**
	 * Given a word, method prints out all institutions whose nicknames
	 * contains the word.   e.g. sending in "Big" would print out:
	 * Cornell University     Big Red
	 * Dartmouth College      Big Green
	 * @param phrase
	 */
	public void findNickname(String phrase) {
		Arrays.stream(list).filter(c -> c.getNickname2().contains(phrase)).forEach(x -> System.out.println(x.getInstitution() + " " + x.getNickname2()));
	}

	/**
	 * Method will print all institutions with a total enrollment larger than a given parameter
	 * @param size
	 */
	public void totalEnrollment(int size) {
		Arrays.stream(list).filter(c -> c.getPop() > size).forEach(x -> System.out.println(x.getInstitution() + " " + x.getPop()));
	}

	/**
	 * Method calculates and returns the average total enrollment (grad plus
	 * undergrad) from all colleges
	 * @return average total enrollment (for all institutions)
	 */
	public double averageEnrollment() {
		return Arrays.stream(list).mapToDouble(c -> c.getPop()).average().orElseThrow(IllegalStateException::new);
	}

	/**
	 * Method will return nickname of institution supplied as a parameter
	 * @param inst
	 * @return nickname of institution
	 */
	public String getNickname(String inst) {
		for(College c: list)
			if(c.getInstitution().equals(inst))
				return c.getNickname2();

		return "false";
	}

	/**
	 * Method calculates the average total enrollment from two given institutions
	 * @param inst1
	 * @param inst2
	 * @return average total enrollment
	 */
	public double twoSchoolAverage(String inst1, String inst2) {
		College c1, c2;
		c1 = Arrays.stream(list).filter(c -> c.getInstitution().equals(inst1)).findFirst().orElseThrow(IllegalStateException::new);
		c2 = Arrays.stream(list).filter(c -> c.getInstitution().equals(inst2)).findFirst().orElseThrow(IllegalStateException::new);
		return (c2.getPop() + c1.getPop())/2.0;
	}

	/**
	 * Method calculates the faculty/enrollment ratio for all schools, and returns
	 * the name of the institution with the highest faculty/enrollment ratio
	 * @return name of institution
	 */
	public String highestRatio() {
		int large = 0;
		for (int i = 0; i < list.length; i++)
			if (list[i].getRatio() > list[large].getRatio()) large = i;
		return list[large].getInstitution();
	}


	public static void main(String[] args) {
		CollegeDataBase one = new CollegeDataBase();
		one.printSchoolsNicknames();
		one.findNickname("B");
		one.totalEnrollment(10000);
		System.out.println("The average enrollment of all colleges is " + one.averageEnrollment());
		System.out.println("The nickname of Princeton University:  " + one.getNickname("Princeton University"));
		System.out.println("The average enrollment of Cornell and Harvard is:  " + one.twoSchoolAverage("Harvard University", "Cornell University"));
		System.out.println("The school with the highest faculty to student ratio: " + one.highestRatio());
	}

}
