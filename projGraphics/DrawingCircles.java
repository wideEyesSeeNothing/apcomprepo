package projGraphics;

import java.util.Random;
import java.awt.*;
import javax.swing.*;

public class DrawingCircles extends JFrame {
	public DrawingCircles() {
		super("Drawing Circles");
		setSize(800, 800);
		setVisible(true);
		repaint();
	}

	public void paint(Graphics g) {

		super.paint(g);
		Graphics2D g2 = (Graphics2D) g;

		g2.setStroke(new BasicStroke(3));
		g.setColor(Color.blue);
		g.fillRect(400,400,50,100);
	} //end of public void paint

	public static void main(String[] args) 
{

  DrawingCircles application = new DrawingCircles();
  application.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
}

}

