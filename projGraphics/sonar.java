import javax.swing.*;
import java.awt.*;
import java.util.Random;
public class sonar extends JFrame{
    public sonar() {
        super("sonar");
        setSize(800,800);
        setVisible(true);
        repaint();
    }
    public void paint(Graphics g){
        Graphics2D g2 = (Graphics2D) g;
        g2.setColor(Color.white);
        int sleepSec = 30;
        g2.fillRect(0,0,800,800);
        Random r = new Random();
//        g2.setColor(Color.black);
//        g2.drawOval(300,300,200,200);
        for (int i = 0; i < 100; i++) {
            float red = r.nextFloat();
            float green = r.nextFloat();
            float blue = r.nextFloat();
            int startX = r.nextInt(800), startY= r.nextInt(800), radius = r.nextInt(400);
//            radius = ((radius+startX)>800||(startX-radius)<0||(radius+startY)>800||(startY-radius)<0)?((radius+startX)>800||(startX-radius)<0)?(radius-Math.abs(800-(startX+radius))):(radius-Math.abs(800-(radius+startY))):radius;
            g2.setColor(new Color(red,green,blue));
            for (int j = 0; j < radius; j+=5) {
                g2.drawOval(startX-j/2, startY-j/2, j, j);
                try {
                    Thread.sleep(sleepSec);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            g2.setColor(Color.white);
            for (int j = 0; j<radius ; j+=5) {
                g2.drawOval(startX-j/2,startY-j/2,j,j);
                try {
                    Thread.sleep(sleepSec);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    public static void main(String[] args){
        sonar app = new sonar();
        app.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
