import javax.swing.*;
import java.awt.*;
public class spiral extends JFrame {
    public spiral() {
        super("spiral");
        setSize(800,800);
        setVisible(true);
        repaint();
    }
    public void paint(Graphics g){
        Graphics2D g2 = (Graphics2D) g;
        g2.setColor(Color.white);
        g2.fillRect(0,0,800,800);
        g2.setColor(Color.red);
        int startX = 400, startY = startX;
        for (int i = 0; i < 400; i+=2) {
            switch ((i/2)%4){
                case 0:
                    if(i==0)
                        g2.drawLine(startX,startY,startX+=2,startY);
                    else
                        g2.drawLine(startX,startY,startX+=i,startY);
                    break;
                case 1:
                    g2.drawLine(startX,startY,startX,startY+=i);
                    break;
                case 2:
                    g2.drawLine(startX,startY,startX-=i,startY);
                    break;
                case 3:
                    g2.drawLine(startX,startY,startX,startY-=i);
                    break;
            }
            try {
                Thread.sleep(25);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    public static void main(String[] args){
        spiral app = new spiral();
        app.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
