import java.util.Random;
import java.awt.geom.*;
import java.awt.*;
import javax.swing.*;
public class pipes extends JFrame{
    public pipes() {
        super("pipes");
        setSize(800,800);
        setVisible(true);
		repaint();
    }

    public void paint(Graphics g){
        super.paint(g);
        Graphics2D g2 = (Graphics2D) g;
        g2.setColor(Color.black);
        g2.fillRect(0,0,800,800);
        Random r = new Random(1234);
        Point2D[] p2p = new Point2D[15];

        for (int i = 0; i < 20; i++) {
            g2.setColor(Color.white);
            for (int j = 0; j < p2p.length; j++) {
                p2p[j] = new Point2D.Double(r.nextInt(800), r.nextInt(800));
                g2.draw(new Line2D.Double(p2p[(j - ((j == 0) ? (0) : (1)))], p2p[j]));
                try {
                    Thread.sleep(100);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            g2.setColor(Color.black);
            for(int j = 0; j < p2p.length-1; j++){
                g2.draw(new Line2D.Double(p2p[j],p2p[j+1]));
                try {
                    Thread.sleep(100);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void main(String[] args){
        pipes app = new pipes();
        app.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}