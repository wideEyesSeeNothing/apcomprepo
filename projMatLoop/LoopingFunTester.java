import java.util.Random;
public class LoopingFunTester {
	public int binary(int bin){
//	return Integer.parseInt(Integer.toBinaryString(prebin));  You don't like fun
		int Final = 0;
		int prebin = bin;
		for (int i = (int)Math.floor(Math.log(prebin)/Math.log(2)); i >=0; i--) {
			if((bin - Math.pow(2,i))<0)
				continue;
			else
				Final += Math.pow(10,i);
			bin -= Math.pow(2,i);
		}
		return Final;
		}
	public void coinTrials(int n){
		Random r = new Random();
		int heads = 0,tails = 0;
		for (int i = 0; i <= n; i++) {
			if(r.nextFloat() > 0.5)
				heads++;
			else
				tails++;
		}
		System.out.printf("Heads: %d \nTails: %d\n", heads,tails);
	}
	public int CountDigits(int n){
		return (int)Math.floor(Math.log10(n));
	}
	public int factorial(int n){
		return (n==0)?(1):(n*factorial(n-1));
	}
	public int fibonacci(int n){
		return (n<=1)?(n):(fibonacci(n-1) + fibonacci(n-2));
	}
	public int gcf(int a,int b){
		int r;
		while(r !=0){
			r = a%b;
			a=b;
			b=r;
		}
		return a;
	}
	public boolean isEven(int n){
		return n % 2 == 0;
	}
	public boolean isPerfect(int n, int s, int i){
		return (i<n)?(isPerfect(n, (n%i!=0)?s+i:s, i+1)):n==s;
	}
	public boolean isPrime(int n){
		for (int i = 2; i < (int)Math.ceil(Math.sqrt(n)); i++) 
			if(n % i == 0)
				return false;
		return true;
	}


// put this in your main method
		public static void main(String[] args) {
			System.out.println("#15 - Binary of 122:  " + binary (122));
			System.out.println("\n#10 - Coin trials:  " );
			coinTrials(100);
			System.out.println("\n#11 - CountDigits of 23456:  " + countDigits (23456));
			System.out.println("\n#3 - Factorial of 10:  " + factorial (10));
			System.out.println("\n#12 - Fibonacci of 10:  " );
			fibonacci (10);
			System.out.println("\n\n#7 - gcf of 32 & 80:  " + gcf(32,80) );
			System.out.println("\n#4 - isEven of 62875:  " + isEven (62875));
			System.out.println("#4 - isEven of 62878:  " + isEven (62878));
			System.out.println("\n#13 - isPerfect of 28:  " + isPerfect (28,0,1));
			System.out.println("#13 - isPerfect of 30:  " + isPerfect (30,0,1));
			System.out.println("\n#5 - isPrime of 30:  " + isPrime (30));
			System.out.println("#5 - isPrime of 31:  " + isPrime (31));
			System.out.println("\n#6 - lcm of 32 & 80:  " + lcm(32,80) );
			System.out.println("\n#14 - Prime Factorization of 112:  " );
			primeFactorization (112);
			System.out.println("\n\n#1 - Prime number list for 100:  " );
			primeNumberList (100);
			System.out.println("\n\n#9 - Reverse of 1358642:  " + reverseNum (1358642));
			System.out.println("\n#16 - Pyramid - 6 high:  " );
			pyramid (6);
			System.out.println("\n#8 - sum digits of 24678:  " + sumDigits (24678));
			System.out.println("\n#2 - sum of ints up to 10:  " + sumInts (10));
				}

	}

}
