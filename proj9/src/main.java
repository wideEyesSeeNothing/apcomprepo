public class main {
    public static void main(String[] args) {
        test("tacocat");
        test("taco cat");
    }
    public static void test(String test){
        Word word = new Word(test);
        System.out.println(word.removeVowels());
        System.out.println(word.reverse());
        System.out.println(word.getNonSpaceLength());
        System.out.println(word.isPalindrome());
        System.out.println(word.isPalindrome2());
        System.out.println(word.anagram());
    }
}
