import java.util.Random;

public class Word {
    private String original;
    /**
     * constructs a Word with String value s
     * @param s is string value of Word
     */
    public Word(String s)
    {
        this.original = s;
    }

    /**
     * reverses letters in original string
     * @return a string that is a reverse of original
     */
    public String reverse()
    {
        String temp ="";
        for (int i = original.length()-1; i >= 0; i--) {
            temp += original.charAt(i);
        }
        // code goes here
        return temp;
    }
    /**
     * returns the number of non-space symbols/characters of a String
     */
    public int getNonSpaceLength()
    {
        return original.replaceAll(" ","").length();
    }

    /**
     * determines is word is a palindrome
     * @return true if word is a palindrome, false otherwise
     */
    public boolean isPalindrome()
    {
        return original.toLowerCase().equals(reverse().toLowerCase());
    }

    /**
     * Alternate method to determine if word is a palindrome
     * @return true if word is a palindrome, false otherwise
     */
    public boolean isPalindrome2()
    {
        for(int i =0,j=original.length()-1; i<original.length() &&j>=0; i++){

            if (original.charAt(j) == original.charAt(i)) {
                j--;
                continue;
            }else
                return false;

        }
        return true;
    }
    /**
     * removes vowels in original string
     * @return a string that removes all of the vowels
     */
    public String removeVowels()
    {
        return original.toLowerCase().replaceAll("a|i|o|e|u", "");
    }

    /**
     * creates an anagram
     * @return a string that is a random anagram of the original word
     */
    public String anagram()
    {
        Random r = new Random();
        String temp = "";
        StringBuilder st = new StringBuilder(original);
        for (int i = 0; i < original.length(); i++) {
            int rand = r.nextInt(st.length());
            temp += st.charAt(rand);
            st.deleteCharAt(rand);
        }
        return temp;
    }

}
