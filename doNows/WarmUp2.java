import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
public class WarmUp2 {


	
	// This method will return a String array which
	// contains the elements of a supplied array
	// with more than one word.
	// The new array should be no larger than necessary.
	public static String[] moreThanOneWord(String[] a) {
		int c = 0;
		for (String s:a) c += (s.contains(" "))?(int)s.chars().filter(ch -> ch == ' ').count():1;
		List<String> tot = new ArrayList<String>(c);
		tot.addAll(Arrays.asList(a));
		for (int i = 0; i < tot.size(); i++) {
			String temp = tot.get(i);
			if(temp.contains(" ")){
				tot.remove(i);
				tot.addAll(i,Arrays.asList(temp.split(" ")));
			}
		}
		return tot.toArray(new String[tot.size()]);
	}

	
	
	
	
	
	public static void main(String[] args) {
		String[] list = { "New Jersey", "Pennsylvania", "New York City", "Delaware", "California", "Idaho", "Illinois",
				"New Mexico", "North Carolina", "Florida" };
		String[] b = moreThanOneWord(list);
		System.out.println(Arrays.toString(b));
	}

}
