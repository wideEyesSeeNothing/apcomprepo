package proj1;
//Alex Valentino
import java.util.Arrays;

public class House {
    private String familyName, address;
    private int kidNum;
    private String[] pets = {"None"};
    public House(String famnam, int childNum, String address){
        familyName = famnam;
        kidNum = childNum;
       this.address = address;
    }
    public House(String famnam, int childNum, String address, String[] pets){
        familyName = famnam;
        kidNum = childNum;
        this.pets = pets;
        this.address = address;
    }
    
    public void famlyInfo(){
        System.out.println("FAMILY: " + String.format("%-8s",familyName) + "# OF KIDS: " + kidNum + " ADDRESS: " + 
        address + "    " + "DOG/CAT:" + String.format(Arrays.toString(pets)).replace('[',' ').replace(']',' '));
    }
}