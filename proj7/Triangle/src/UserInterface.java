package Triangle.src;

import java.awt.geom.Point2D;

public class UserInterface {
    public static void main(String args[]){
        Point2D[] points = {new Point2D.Double(3,9), new Point2D.Double(8,0), new Point2D.Double(5,10)};
        Triangle tri = new Triangle(points);
        System.out.println("The triangle with vertices at:");
        for(int i = 0; i < 3; i++)
            System.out.printf("\t Point %c : (%.2f, %.2f)\n", (char)(i+97), points[i].getX(), points[i].getY());
        System.out.println("Has the following properties:");
        System.out.println("Length of side AB: " + tri.getSides()[0]);
        System.out.println("Length of side BC: " + tri.getSides()[1]);
        System.out.println("Length of side AC: " + tri.getSides()[2]);
        for(int i = 0; i < 3; i++)
            System.out.printf("\t Angle %c : %.2f\n", (char)(i+65), Math.toDegrees(tri.getAngles()[i]));  
        //System.out.printf("\t Angle %c : %.2f\n", (char)(i+65), Math.toDegrees(tri.getAngles()[((i+1) % 3)]));
        System.out.println("With a perimeter of: " + tri.getPerimeter());
        System.out.println("And an area of: " + tri.getArea());
        System.out.println("Triangle is Isosceles: " + tri.isIsosceles());
        System.out.println("Triangle is Equilaterial: " + tri.isEquilateral());
        System.out.println("Triangle is Scalene: " + tri.isScalene());
        System.out.println("Triangle is Right: " + tri.isRight());
        System.out.println("Triangle is Acute: " + tri.isAcute());
        System.out.println("Triangle is Obtuse: " + tri.isObtuse());
        
        System.out.println("Circumcenter: " + "(" + tri.getCircumcenter().getX() + ")" + "(" + tri.getCircumcenter().getY() + ")");
        System.out.println("Centroid: " + "(" + tri.getCentroid().getX() + ")" + "(" + tri.getCentroid().getY() + ")");
        System.out.println("Orthocenter: " + "(" + tri.getOrthocenter().getX() + ")" + "(" + tri.getOrthocenter().getY()+ ")");
    }
}
