package Triangle.src;

import java.awt.geom.Point2D;

import Triangle.src.LinearEQ;

public class Triangle{
    private Point2D[] points = new Point2D.Double[3];
    private Double[] sides = new Double[3];
    private Double[] angles = new Double[3];
    private double area;
    private double perimeter;
    private Point2D centroid = new Point2D.Double(0,0);
    private Point2D circumcenter = new Point2D.Double(0,0); //Hehe, cum
    private Point2D orthocenter = new Point2D.Double(0,0);

    public Triangle(Point2D[] points) {
//        if(points.length > 3){
//            throw new IOException("Too many points for a Triangle");
//        }
        this.points = points;
        for(int i = 0; i < points.length; i++)
            sides[i] = (i < 2) ? (points[i].distance(points[i+1])) :
                    (points[i].distance(points[0]));

        for(int i = 0; i < sides.length; i++){
                // angle a = b^2 + c^2 -a^2 / 2bc 
                // angle b = c^2 + a^2 -b^2 / 2ca
                //angle c = a^2 + b^2 - c^2 / 2ab
                int b = (i+1) % 3;
                int c = (i+2) % 3;
                angles[((i+2)%3)] = Math.acos((Math.pow(sides[b], 2) + Math.pow(sides[c], 2) 
                - Math.pow(sides[i], 2)) / (2*sides[b]*sides[c]));
                // System.out.printf("%c %c %c\n", (char)(b+97), (char)(c+97), (char)(i+97));    
        }
        
        perimeter = sides[0] + sides[1] + sides[2];
        double s = perimeter * 0.5;
        area = Math.sqrt(s * (s - sides[0]) * (s - sides[1]) * (s - sides[2]));
        perimeter = sides[0] + sides[1] + sides[2];
        centroid.setLocation((points[0].getX()+points[1].getX()+points[2].getX())/3,
                (points[0].getY()+points[1].getY()+points[2].getY())/3);
        
        Double[] slopes =  new Double[2];
        for (int i = 0; i < slopes.length; i++)
            slopes[i] = -1 * (points[i+1].getX() - points[i].getX())/(points[i+1].getY() - points[i].getY());
        // System.out.print(slopes[0] + " " + slopes[1]);
        //Have to get the slopes into the form from a*(x-c) + d = y to   ax - y = ca - d
        Point2D[] midpoints = {new Point2D.Double(0,0), new Point2D.Double(0,0)};
        for (int i = 0; i < midpoints.length; i++)
            midpoints[i].setLocation((points[i].getX() + points[i+1].getX())/2,(points[i].getY() + points[i+1].getY())/2);
        // System.out.println(midpoints[0].getX() + " " + midpoints[0].getY() + " " + midpoints[1]);
        LinearEQ[] equa = new LinearEQ[2];
        for (int i = 0; i < slopes.length; i++)
            equa[i]= new LinearEQ(-slopes[i], 1, (midpoints[i].getX()*slopes[i] - midpoints[i].getY()));
        circumcenter.setLocation(equa[0].solution(equa[1]));
        
        for(int i = 0; i < slopes.length; i++)
            equa[i] = new LinearEQ(-slopes[i], 1, points[((i+2)%3)].getX()*slopes[i] - points[((i+2)%3)].getY());
        orthocenter.setLocation(equa[0].solution(equa[1]));
    }

    public Double[] getSides() {
        return sides;
    }

    public Double[] getAngles() {
        return angles;
    }

    public double getArea() {
        return area;
    }

    public double getPerimeter() {
        return perimeter;
    }

    public boolean isIsosceles(){
        return angles[0].equals(60.0) && angles[1].equals(60.0);
    }

    public boolean isEquilateral(){
        return angles[0].equals(angles[1]) || angles[1].equals(angles[2]);
    }

    public boolean isScalene(){
        return !angles[0].equals(angles[1]) && !angles[1].equals(angles[2]);
    }

    public boolean isRight(){
        return angles[0] == 90 || angles[1] == 90 || angles[2] == 90;
    }

    public boolean isAcute(){
        return angles[0] < 90 && angles[1] < 90 && angles[2] < 90;
    }

    public boolean isObtuse(){
        return angles[0] > 90 || angles[1] > 90 || angles[2] > 90;
    }

    public Point2D getCentroid() {
        return centroid;
    }

    public Point2D getCircumcenter() {
        return circumcenter;
    }

    public Point2D getOrthocenter(){
        return orthocenter;
    }
}