package Triangle.src;

import java.awt.geom.Point2D;

public class LinearEQ {

	public double xcoeff, ycoeff, constant;

	// default constructor (write signature line and implement)
	public LinearEQ(double xcoeff, double ycoeff, double constant){
		this.xcoeff = xcoeff;
		this.ycoeff = ycoeff;
		this.constant = constant;
	}
	
	
	// overloaded constructor takes an x-coefficient, y-coefficient, and constant
	// for linear equation of form ax + by + c = 0
	// (write signature line and implement)

	
	
	// method to round numbers to two decimals
	// (this was covered previously - use this to clean up your points)
	public double clean(double d){
		return Double.parseDouble(String.format("%.2f", d));
	}
	// returns the x-value of the intersection of two lines
	public double xSol(LinearEQ b){
		return (this.ycoeff * b.constant - this.constant * b.ycoeff)/(this.xcoeff * b.ycoeff - b.xcoeff * this.ycoeff);
	}

	// returns the y-value of the system's intersection

	public double ySol(LinearEQ b){
		return (this.xcoeff * b.constant - this.constant * b.xcoeff)/(this.ycoeff * b.xcoeff - b.ycoeff * this.xcoeff);
	}

	// returns a Point object (or myPoint object) that represents
	// the coordinate where the two lines intersect
	public Point2D solution(LinearEQ b){
		Point2D solu = new Point2D.Double();
		solu.setLocation(this.clean(this.xSol(b)), this.clean(this.ySol(b)));
		return solu;
	}
	
	

/*  	public static void main(String[] args) {
		// example calls for constructors
		LinearEQ one = new LinearEQ(2, -3, -2);
		LinearEQ two = new LinearEQ(4, 1, 24);
		System.out.println(one.solution(two));
		System.out.println(two.solution(one));
		//include testers that can take objects one and two
		//above and print out the solution to the system
		
		
		
	}*/

}
