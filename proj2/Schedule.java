public class Schedule {
    private String teacher, subject;

    public Schedule(String teacher, String subject){
        this.teacher = teacher;
        this.subject = subject;
    }

    public String getClassInfo(){
        return ("TEACHER: " + teacher + " SUBJECT: " + subject);
    }
}