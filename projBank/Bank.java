import java.util.ArrayList;
import java.util.List;
public class Bank {
    List<BankAccount> bankSheet = new ArrayList<BankAccount>();
    public void addAccount(BankAccount a){
        bankSheet.add(a);
    }
    public int count(double amount){
       return (int)bankSheet.stream().filter(b -> b.getBalance() >= amount).count();
    }
    public BankAccount find(int accountNumber){
        return bankSheet.stream().filter(b -> b.anAccountNumber==accountNumber).findFirst().orElseThrow(IllegalStateException::new);
    }
    public BankAccount getMaximum(){
        BankAccount temp = bankSheet.get(0);
        for(BankAccount b : bankSheet){
            if(b.getBalance() > temp.getBalance())
                temp = b;
        }
        return temp;
    }
    public double getTotalBalance(){
        return bankSheet.stream().mapToDouble(BankAccount::getBalance).sum();///bankSheet.size();
    }
}   
