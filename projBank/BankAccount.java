public class BankAccount{
    int anAccountNumber;
    double initialBalance;

    public BankAccount(int anAccountNumber, double initialBalance) {
        this.anAccountNumber = anAccountNumber;
        this.initialBalance = initialBalance;
    }

    public BankAccount(int anAccountNumber){
        anAccountNumber = this.anAccountNumber;
    }
    public void deposit(double depo){
        initialBalance += depo;
    }
    public void withdraw(double with){
        initialBalance -= with;
    }
    public int getAccountNumber(){
        return anAccountNumber;
    }
    public double getBalance(){
        return initialBalance;
    }
    public String toString(){
        return "Acct# " + anAccountNumber + " : $" + initialBalance;
    }
}