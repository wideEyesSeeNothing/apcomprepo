public class GuidanceGroup {
    // Array group will store SatStudent objects
    private SatStudent[] group;
    /**
    * Default Constructor for objects of class GuidanceGroup
    */
    public GuidanceGroup() {
    group = new SatStudent[12];
    group[0] = (new SatStudent("Ye", 640, 695, 686));
    group[1] = (new SatStudent("Bradley", 778, 768, 780));
    group[2] = (new SatStudent("Chen", 687, 614, 705));
    group[3] = (new SatStudent("Davis", 620, 534, 556));
    group[4] = (new SatStudent("Aarons", 550, 565, 517));
    group[5] = (new SatStudent("Gupta", 687, 720, 640));
    group[6] = (new SatStudent("Park", 722, 721, 745));
    group[7] = (new SatStudent("Kohl", 595, 605, 615));
    group[8] = (new SatStudent("Mehta", 525, 637, 521));
    group[9] = (new SatStudent("Bahl", 611, 607, 610));
    group[10] = (new SatStudent("Smith", 670, 703, 610));
    group[11] = (new SatStudent("Issacs", 670, 690, 710));
    }

    public SatStudent[] getGroup() {
        return group;
    }

    // Constructor with a SatStudent[] array as a parameter
    public GuidanceGroup(SatStudent[] myGroup) {
    group = myGroup;
    }
    
    // This method displays a chart with 2 columns: Name and Total SAT score
    public void display() {
    for(SatStudent s: group) System.out.println(s.getName() + " : " + s.getSuperScore());
    }
    // This method returns the name of the student with the lowest total SAT
    public String getMin() {
        SatStudent locMin=group[0];
        for(SatStudent s: group) locMin = (s.getSuperScore() <= locMin.getSuperScore())?s:locMin;
        return locMin.getName();
    }
    // This method returns the name of the student with the highest total SAT
    // score
    public String getMax() {
        SatStudent locMax=group[0];
        for(SatStudent s: group) locMax = (s.getSuperScore() <= locMax.getSuperScore())?s:locMax;
        return locMax.getName();
    }
    // This method returns the average of all SAT scores
    public double average() {
        int sum = 0;
        for(SatStudent s: group) sum+= s.getSuperScore();
        return sum/group.length;
    }
    // This method displays a list of names of students who scored below 1700
    public void below1700List() {
        for(SatStudent s: group) if (s.getSuperScore() < 1700) System.out.println(s.getName() + " : " + s.getSuperScore());
    }
    // This method displays a list of names of students who scored above2000
    public void above2000List() {
        for(SatStudent s: group) if (s.getSuperScore() > 2000) System.out.println(s.getName() + " : " + s.getSuperScore());
    }
    // this method will return the total score of an inputted name.
    // return -1 if student is not found
    public int getScore(String lookfor)
    {
        for(SatStudent s: group) if(s.getName().equals(lookfor)) return s.getSuperScore();
    return -1;
    }
    // returns an array of SatStudents with total scores &gt;1900
    public SatStudent[] scholarship() {
        int c = 0, ind = c;
        for(SatStudent s: group)
            c += (s.getSuperScore() > 1900)?1:0;
        SatStudent[] temp= new SatStudent[c];
        for(SatStudent s: group)
            if(s.getSuperScore() > 1900){
                temp[ind] =s;
                ind++;
            }
        return temp;
    }
    // returns a new GuidanceGroup with the SatStudents of this class combined
    // with the SatStudents of other class
    public GuidanceGroup combineGroups(GuidanceGroup other) {
        SatStudent[] otherOnes = other.getGroup();
        SatStudent[] temp = new SatStudent[otherOnes.length + group.length];
        for (int i = 0; i < group.length; i++)
            temp[i] = group[i];
        for (int i = 0; i < otherOnes.length; i++)
            temp[i+group.length] = group[i];
    return new GuidanceGroup(temp);
    }
    //Sorts the array of the GuidanceGroup by total score (descending)
    public void sortByTotalScore() {
        for (int i = 0; i < group.length-1; i++) {
            for (int j = 0; j < group.length-i-1; j++) {
                if(group[i].getSuperScore() < group[i+1].getSuperScore()){
                    SatStudent temp = group[i];
                    group[i] = group[i+1];
                    group[i+1] = temp;
                }
            }
        }
    }
    //Sorts the array of the GuidanceGroup by Name (ascending (alphabetically))
    public void sortByName() {
        for (int i = 0; i < group.length-1; i++) {
            for (int j = 0; j < group.length-i-1; j++) {
                if(group[i].getName().toLowerCase().charAt(0) > group[i+1].getName().toLowerCase().charAt(0)){
                    SatStudent temp = group[i];
                    group[i] = group[i+1];
                    group[i+1] = temp;
                }
            }
        }
    }
    // this method will return the average of all of the students in the
    // Scholarship category
    public double getScholarShipAverage() {
        int sum = 0, ind = sum;
        for(SatStudent s: group) {
            sum += (s.getSuperScore() > 1900) ? s.getSuperScore() : 0;
            ind += (s.getSuperScore() > 1900) ? 1 : 0;
        }
        return ((double)sum)/ind;
    }
}