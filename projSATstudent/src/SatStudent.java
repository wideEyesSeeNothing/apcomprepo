public class SatStudent {
    public String name;
    public int math;
    public int verbal;
    public int writing;
    public int grade;
    public int superScore = math+verbal+writing;

    public int getSuperScore() {
        return superScore;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMath() {
        return math;
    }

    public void setMath(int math) {
        this.math = math;
    }

    public int getVerbal() {
        return verbal;
    }

    public void setVerbal(int verbal) {
        this.verbal = verbal;
    }

    public int getWriting() {
        return writing;
    }

    public void setWriting(int writing) {
        this.writing = writing;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public SatStudent(String name, int math, int verbal, int writing) {
        this.math = math;
        this.writing = writing;
        this.verbal = verbal;
        this.name = name;
        this.superScore = writing + math +verbal;
//        this("Andersson", 670,680,700);
    }
}