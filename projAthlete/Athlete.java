
/**
 * Defines an athlete.
 */
public abstract class Athlete {

	private String firstName;
	private String lastName;
	private String sport;
	private double hoursTraining = 0;

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public Athlete(String firstName, String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public String getSport() {
		return sport;
	}

	public void setSport(String sport) {
		this.sport = sport;
	}

	public double getHoursTraining() {
		return hoursTraining;
	}

	public void setHoursTraining(double hoursTraining) {
		this.hoursTraining += hoursTraining;
	}

	/**
	 * Constructs an athlete with a specified name and sport; hoursTraining is
	 * Allow sport & hoursTraining to be initialized to default values;
	 * 
	 * @param fname
	 *            first name of athlete
	 * @param lname
	 *            last name of athlete
	 */


	@Override
	public String toString() {
		return "Name: " + firstName + " " + lastName + " is a " +
				sport + " and has trained for " + hoursTraining + " hours, expending "
				+ caloriesBurned() + " calories";
	}

	/**
	 * Increments hours of training.
	 * 
	 * @param hours   number of hours athlete trained
	 */
	public abstract void train(double hours);
	
	/**
	 * Calculates the number of calories burned
	 * based on the number of hours athlete trained
	 * 
	 */
	public abstract int caloriesBurned();


}