public class Marathoner extends Runner{

    public Marathoner(String firstName, String lastName) {
        super(firstName, lastName);
        setSport("Marathoner");
    }
    public void race(int miles){
        if(miles > 10) {
            setRaceNum(1);
            setMiles(miles);

        }else{
            setHoursTraining(miles*(1/60.0)*8.5);
        }
    }
}