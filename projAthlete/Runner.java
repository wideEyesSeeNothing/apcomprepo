public class Runner extends Athlete{

    private int miles, raceNum;
    public Runner(String firstName, String lastName) {
        super(firstName, lastName);
        setSport("Runner");
    }
    public void race(int miles){
        this.miles += miles;
        raceNum++;
    }
    @Override
    public void train(double hours) {
        setHoursTraining(hours);
    }

    public void setMiles(int miles) {
        this.miles += miles;
    }

    public void setRaceNum(int raceNum) {
        this.raceNum += raceNum;
    }

    @Override
    public int caloriesBurned() {
        return (int)(getHoursTraining() * 400.0);
    }
    public String toString(){
        return "Name: " + getFirstName() + " " + getLastName() + " is a " +
                getSport() + " and has trained for " + getHoursTraining() + " hours, expending "
                + caloriesBurned() + " calories" + '\n' + "# OF RACES & TRAINING MILES: "
                + raceNum + " races / " + miles + " miles";
    }
}