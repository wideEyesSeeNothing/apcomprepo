public class Wrestler extends Athlete{
    public Wrestler(String firstName, String lastName) {
        super(firstName, lastName);
        setSport("Wrestler");
    }

    @Override
    public void train(double hours) {
        setHoursTraining(hours);
    }

    @Override
    public int caloriesBurned() {
        return (int)getHoursTraining()*600;
    }

}
