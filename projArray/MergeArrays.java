public class MergeArrays {
    // displays the array
	public static void display( int[] a)
	{
        for(int x: a)System.out.println(x);
	}

	// returns a new array where b is appended to the end of a
	public static int[] append (int[] a , int[] b)
	{
        int[] temp = new int[a.length+b.length];
        for (int i = 0; i < a.length; i++) 
            temp[i] = a[i];
        for (int i = 0; i < b.length; i++)
            temp[a.length+i] = b[i]; 
		return temp;
	}

	// returns a new array with alternating values from a and b
	// (question: does it matter if the arrays have different lengths?
	public static int[] alternate( int[]a, int[] b) {
		int aC=0,bC=aC;
		int[] temp = new int[a.length+b.length];
		for (int i = 0; i < ((a.length < b.length)?a.length:b.length); i++) {
			if(i % 2==0){
				temp[i] = b[bC];
				bC++;
			}	
			else{
				temp[i] = a[aC];
				aC++;
			}
		}
		for (int i = ((a.length < b.length)?bC:aC); i < ((a.length < b.length)?b.length:a.length); i++) {
			temp[(i+((a.length < b.length)?a.length:b.length))] = ((a.length < b.length)?b[i]:a[i]);
		}
		return temp;
	}

	//returns a new ordered array
	// prerequisite a and b must be ordered
	// (and NO, you may NOT just append the arrays and then sort – individual
	// elements must be compared between the arrays to see what to add next)
	public static int[] merge( int[] a, int[] b)
	{
		int aC=0,bC=aC;
		int[] temp = new int[a.length+b.length];
		for (int i = 0; i < ((a.length < b.length)?a.length:b.length); i++) {
			if(a[aC] > b[bC]){
				temp[i] = b[bC];
				bC++;
			}
			else{
				temp[i] = a[aC];
				aC++;
			}
		}
		for (int i = ((a.length < b.length)?bC:aC); i < ((a.length < b.length)?b.length:a.length); i++) {
			temp[(i+((a.length < b.length)?a.length:b.length))] = ((a.length < b.length)?b[i]:a[i]);
		}
		return temp;
	}

	public static void main(String[] args) {

		int[] a = new int[]{1,4,9,16,25,36,49,64,81,100};
		int[] b = new int[] {3,10,11,12,21,26};
		System.out.println("APPEND") ;
		display(append(a,b));
		System.out.println() ;
		System.out.println("ALTERNATE") ;
		display(alternate(a，b));	
		System.out.println() ;
		System.out.println("MERGE") ;
		display(merge(a,b));
	}
}