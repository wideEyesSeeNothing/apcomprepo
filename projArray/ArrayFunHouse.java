public class ArrayFunHouse
{
	//instance variables and constructors could be used, but are not really needed

	//getSum() will return the sum of the numbers from start to stop, not including stop
	public static int getSum(int[] numArray, int start, int stop)
	{
		int sum = 0;
		for (int i = start; i < stop; i++)
			sum += numArray[i];
		return sum;
	}

	//getCount() will return number of times val is present
	public static int getCount(int[] numArray, int val)
	{
		int occur = 0;
		for (int i = 0; i < numArray.length; i++) 
			occur += (numArray[i] == val)?(1):(0);
		return occur;
	}

	public static int[] removeVal(int[] numArray, int val)
	{
		int[] z = new int[getCount(numArray, val)];
		int diff = 0;
		for (int i = 0; i < z.length; i++) 
			if(numArray[i] != val)
				z[i] = numArray[i+diff];
			else 
				diff++; 
		return z;
	}
}