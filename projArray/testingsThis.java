public class testingsThis {
    public static void main(String[] args) {
        ArrayExample ae = new ArrayExample();
        ae.display();
        System.out.println("Reverse display:");
        ae.displayReverse(0);
        System.out.println("AVG " + ae.average(0,0));
        System.out.println("Linear search");
        System.out.println(ae.linearSearch(4,0));
        System.out.println("Find max" );
        System.out.println(ae.findMax(0,0));
        System.out.println("Tally list:" );
        ae.tallyList(0);
        System.out.println("reverse tally list:");
        ae.listReverseAvg(0);
        System.out.println("sorted array");
        ae.sort();
        ae.display();
    }
}
