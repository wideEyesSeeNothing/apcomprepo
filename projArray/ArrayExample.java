public class ArrayExample {
    private int[] n = new int[]{1,3,7,19,15,19,7,3,19,48};
    public ArrayExample(){}
    public ArrayExample(int c){n=new int[c];for(;c>0;c--)n[c-1]=(int)(24*Math.random());}
    private int l = n.length-1;
    public void display(){for(int x:n)System.out.println(x);} 
    public void displayReverse(int p){System.out.println(n[l-p]);if((l-p)!=0)displayReverse(p+1);}
    public double average(int s,int c){return c<l?average(s+=n[c],c+1):s/(double)l;}
    public int findMax(int localMax,int c){return c<l+1?localMax>n[c]?findMax(localMax,c+1):findMax(n[c],c+1):localMax;}
    public int linearSearch(int lookFor, int c){return c<l+1?lookFor!=n[c]?linearSearch(lookFor,c+1):c:-1;}
    public void tallyList(int c){System.out.println(n[c]+" "+n[l-c]+" "+(n[c]+n[l-c]/2.0));if(c<l)tallyList(c+1);}
    public void listReverseAvg(int c){System.out.println(n[l-c]+" "+n[c]+" "+(n[c]+n[l-c]/2.0));if(c<l)tallyList(c+1);}
    public void sort(){
        for (int i = 0; i <= n.length; i++) {
            for (int j = 1; j < n.length; j++) {
                if(n[j-1]>n[j]){
                    int temp = n[j-1];
                    n[j-1] = n[j];
                    n[j] = temp;
                }
            }
        }
    }
}