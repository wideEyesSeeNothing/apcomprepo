import java.util.Arrays;
import java.util.Scanner;
public class oddsNevens {
    public static int[] counting(int numbers[]){
        int odd = 0, even = odd;
        for (int i = 0; i < numbers.length; i++) {
            odd += (numbers[i] % 2 == 0)?0:1;
            even += (numbers[i] % 2 == 0)?1:0;
        }
        return new int[]{odd,even};
    }
    public static int[][] sort(int numbers[]){
        int oddNeven[] = counting(numbers); 
        int[] odds = new int[oddNeven[0]], evens = new int[oddNeven[1]];
        int i1 = 0, i2 = i1;
        for (int i = 0; i < numbers.length; i++) {
            if(numbers[i] % 2 == 0){
                evens[i2] = numbers[i];
                i2++;
            } else {
                odds[i1] = numbers[i];
                i1++;
            }
        }
        return new int[][]{odds,evens};
    }
    public static void main(String args[]){
        System.out.println("put in the number of digits you want to put in: ");
        Scanner scan = new Scanner(System.in);
        int lim = scan.nextInt();
        int[] numbers = new int[lim];
        for (int i = 0; i < lim; i++) {
            System.out.println("Put in number: " + (i + 1));
            numbers[i] = scan.nextInt();
        }
        System.out.println("Odds: " + Arrays.toString(sort(numbers)[0]));
        System.out.println("Evens: " + Arrays.toString(sort(numbers)[1]));
        scan.close();
    }

}
