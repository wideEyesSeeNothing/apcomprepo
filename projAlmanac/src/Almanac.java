public class Almanac {
    City[] list;
    public Almanac() {
        list = new City[5];
        list[0] = new City(1581000, "Philadelphia");
        list[1] = new City(486290 , "Atlanta");
        list[2] = new City(486290 , "Boston");
        list[3] = new City(8623000, "New York");
        list[4] = new City(28491, "West Windsor");
    }

    /**
     * Displays List of Cities
     */
    public void printList() {
        for(City c: list) System.out.println(c.getName() + " : " + c.getPopulation());
    }

    /**
     * Find the city with the smallest population return the name.
     */
    public String smallestPop() {
        City temp = list[0];
        for(City c: list)
            if(temp.getPopulation() > c.population)
                temp = c;
        return temp.name;
    }
    /**
     * Returns the average population
     */
    public double averagePop() {
        int C = 0;
        for(City c: list) C += c.getPopulation();
        return C/list.length;
    }
    /**
     * Changes the population of a City that matches searchName to newPopulation
     */
    public void changePop(String searchName, int newPopulation) {
        for (int i = 0; i < list.length; i++)
            if(list[i].getName().equals(searchName))
                list[i].setPopulation(newPopulation);
    }

    /**
     * this method will add rate % to each population in the list
     * e.g. increasePop(5) will increase a city&#39;s population by 5%
     */
    public void increasePop(double rate) {
        for (int i = 0; i < list.length; i++)
            list[i].setPopulation((int)(list[i].getPopulation() * (1+(rate/100))));
    }
    public static void main(String[] args) {
        Almanac one = new Almanac();
        one.printList();
        System.out.println();
        System.out.println("Average Population: " + one.averagePop());
        System.out.println("Smallest: " + one.smallestPop());
        one.changePop("West Windsor", 40000);
        System.out.println();
        System.out.println("After Change");
        one.printList();
        one.increasePop(5);
        System.out.println();
        System.out.println("After Increase");
        one.printList();
    }
}
