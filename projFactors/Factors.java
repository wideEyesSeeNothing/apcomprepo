import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import static java.lang.System.*;

public class Factors
{
	public static ArrayList<Integer> getListOfFactors(int number)
	{
		ArrayList<Integer> temp = new ArrayList<Integer>(List.of(1,number));
		int bound = (int)Math.ceil(number/2.0);
		for (int i = 2; i <= bound; i++)
			if(number % i == 0)
				temp.add(i);
		return temp;
	}
	
	public static void keepOnlyCompositeNumbers( List<Integer> nums )
	{
		for(int n = 0; n < nums.size(); n++){
			if(getListOfFactors(nums.get(n)).size() <= 2)
				nums.remove(n);
		}
	}
}