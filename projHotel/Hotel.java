public class Hotel {
    // list[] stores reservations for each room. The index is the room number.
    // All elements are null at the beginning
    private Reservation[] list;
        public Hotel(int numbRooms) {
    list = new Reservation[numbRooms];
    }
    // Displays room numbers and reservation information.
    public void display() {
        for (int i = 0; i < list.length; i++) {
            System.out.println((i+1) + " : " + list[i]);
        }
    }
    
    // returns true if room is occupied
    public boolean isOccupied(int room) {
    return !(list[room]==null);
    }
    // Assigns a new Reservation to the first available room.
    // Print out reservation with room number or indicate that hotel is full
    public void reserveRoom(String name, double rate) {
        int i = reservedRoomCount();
        if(i == list.length)
            System.out.println("Leave, now, or we're calling the cops on you degenerate(s).  Thank you for considering the Hilton Inn");
        else{
        list[i] = new Reservation(name, rate);
        }
    }
    // Assigns a new Reservation to an inputted room.
    // if room is occupied, find the first available room in the array
    // Print out reservation with room number or indicate that hotel is full
    public void reserveRoom(String name, double rate, int room) {
        if(isOccupied(room))
            reserveRoom(name, rate);
        else
        list[room-1] = new Reservation(name, rate);
    }
    // Find and return number of rooms with a reservation
    public int reservedRoomCount() {
        int c = 0;
        for(Reservation r: list) c += (r==null) ? 0:1;
        return c;
    }
    // find and return the average room rate of the occupied rooms.
    public double averageRoomRate() {
        int c = reservedRoomCount();
        double avg = 0.0;
        for (int i = 0; i < c-1; i++)
            avg += list[i].getRate();
        return avg/c;
    }
}