package MagpieActivityStarterCode.activity3;

/**
 * A program to allow students to try out different 
 * String methods. 
 * @author Laurie White
 * @version April 2012
 */
public class StringExplorer
{

	public static void main(String[] args)
	{
		String sample = "The quick brown fox jumped over the lazy dog.";

		//  Demonstrate the indexOf method.
		int position = sample.indexOf("quick");
		System.out.println ("sample.indexOf(\"quick\") = " + position);

		//  Demonstrate the toLowerCase method.
		String lowerCase = sample.toLowerCase();
		System.out.println ("sample.toLowerCase() = " + lowerCase);
		System.out.println ("After toLowerCase(), sample = " + sample);

		//  Try other methods here:
		MagpieActivityStarterCode.activity3.Magpie3 mp = new MagpieActivityStarterCode.activity3.Magpie3();
		System.out.println(mp.findKeyword("yesterday is today's day before.", "day", 0));
		System.out.println(mp.findKeyword("She's my sister", "sister", 0));
		System.out.println(mp.findKeyword("Brother Tom is helpful", "brother", 0));
		System.out.println(mp.findKeyword("I can't catch wild cats.", "cat", 0));
		System.out.println(mp.findKeyword("I know nothing about snow plows.", "no", 0));
	}

}
