public class RetailItem {
    private String description;
    private float price;
    private int unitsOnHand;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getUnitsOnHand() {
        return unitsOnHand;
    }

    public void setUnitsOnHand(int unitsOnHand) {
        this.unitsOnHand = unitsOnHand;
    }

    public RetailItem(String description, float price, int unitsOnHand) {
        this.description = description;
        this.price = price;
        this.unitsOnHand = unitsOnHand;
    }
}
