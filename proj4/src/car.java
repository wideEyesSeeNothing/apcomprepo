public class car {
    private int yearModel, speed;

    public int getYearModel() {
        return yearModel;
    }

    public void setYearModel(int yearModel) {
        this.yearModel = yearModel;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public String getMake() {
        return Make;
    }

    public void setMake(String make) {
        Make = make;
    }

    private String Make;
    public car(int yearModel, int speed, String Make){
        this.Make = Make;
        this.yearModel = yearModel;
        this.speed = speed;
    }
    public void accelerate(){
        speed += 5;
    }
    public void brake(){
        speed -= 5;
    }

}
