import java.util.Scanner;

public class Demo {
    public static void main(String[] args) {
        car Car = new car(2012, 60, "Ford Torus");
        for(int i = 0; i <5; i++){
            Car.accelerate();
            System.out.println(Car.getSpeed());
        }
        for(int i = 0; i < 5; i++){
            Car.brake();
            System.out.println(Car.getSpeed());
        }
        personalInfo me = new personalInfo("Alex", "123 apple st.", 17,7777777777l);
        personalInfo ma = new personalInfo("Ma", "123 apple st.", 69, 7777777776l);
        personalInfo pa = new personalInfo("Pa", "123 apple st.", 69, 7777777775l);
        RetailItem jacket = new RetailItem("Jacket", 59.95f, 12);
        RetailItem jeans = new RetailItem("Designer Jeans", 34.95f, 40);
        RetailItem Shirt = new RetailItem("Shirt", 24.95f, 20);
        Pet pet = new Pet();
        Scanner kb = new Scanner(System.in);
        System.out.println("Put in your pet's name");
        pet.setName(kb.nextLine());
        System.out.println("Put in what type of pet you have");
        pet.setType(kb.nextLine());
        System.out.println("How old is your pet?");
        pet.setAge(kb.nextInt());
        pet.petInfo();
    }
}
