package projPigLatin;
public class pigLatin {
    String s, pLatin;
    public pigLatin(String s) {
        this.s = s;
    }

    public String translate() {
        String[] words = s.split(" ");
        for(String indu:words){
            pLatin += phraseTranslate(indu);
        }
        return pLatin;
    }
    private String phraseTranslate(String input) {
        String unDesirables = "";
        for(char x:input.toCharArray()) {
            if (x == '.' || x == ',' || x == '!' || x == '?') {
                unDesirables += x;
                input = input.substring(0, (input.length() - 1));
            }
        }
        if(isVowel(input.toLowerCase().charAt(0)))
            return input + "yay" + unDesirables + " ";
        else{
            for (int i = 0; i < input.length(); i++) {
                if(isVowel(input.toLowerCase().charAt(i))){
                    String begin = input.substring(0,i),end = input.substring(i);
                    return end + begin + "ay" + unDesirables + " ";
                }
            }
        }
        return input + "ay";
    }
    private boolean isVowel(char ch) {
        return (ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u' || ch == 'y');
    }
    public static void main(String[] args) {
        pigLatin pLatin = new pigLatin("I like apples, bananas, and ice cream");
        System.out.println(pLatin.translate());
    }
}