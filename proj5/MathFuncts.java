import java.awt.Point;
import java.util.concurrent.ThreadLocalRandom;
public class MathFuncts {
// 1. This method returns the distance between integers a and b
public static int distance(int a, int b) {
	return Math.abs(b -a);
}
// 2. This method returns the maximum value of integers a, and b. (the Java API may help.)
public static int maximum(int a, int b) {
	return (b > a) ? b : a;
}
// 3. This method returns the maximum value of double values a,b,c. (the Java API may
//help.)
public static double maximum(double a, double b, double c) {
		return Math.max(Math.max(a,b),Math.max(b,c));
}
// 4. This method will return a random integer between 9 and 12 (inclusive) (the Java API
//may help.)
public static int getRandomClass() {
	return ThreadLocalRandom.current().nextInt(9,12+1);
}
// 5. This method will return the surface area of a sphere with given radius
// SA = 4PIr�
public static double sphereSurfaceArea(int radius) {
	return 4*Math.PI*Math.pow(radius, 2);
}
// 6. This method will return the volume of a sphere with given radius
// V = 4/3PI r^3
public static double sphereVolume(int radius) {
	return (4f/3f)*Math.PI*Math.pow(radius, 3);
}
// 7. This method will return the length of the hypotenuse of a right
// triangle
// with legs leg1 and leg2
public static double hypotenuse(double leg1, double leg2) {
	return Math.sqrt(Math.pow(leg1, 2) + Math.pow(leg2,2));
}
// 8. This method will return the length of Segment AB
// Refer to java's Point class (the Java API may help.)
public static double segmentLength(Point a, Point b) {
	return hypotenuse(b.getX()-a.getX(), b.getY()-a.getY());
}
// 9. this method will find the smallest angle (in degrees) of a right
// triangle with with base and height leg1 and leg2 (the Java API may help.)
public static double getSmallestAngleOfRightTri(double leg1, double leg2) {
	return Math.toDegrees(Math.min(Math.atan(leg2/leg1), Math.toRadians(90) - Math.atan(leg2/leg1)));
}
// 10. This method will round x to the nearest hundredPlace
// roundToHundredPlace(1297) =======&gt; 1300
public static int roundToHundredPlace(int x) {
	return (int) (100*Math.ceil(x * 0.01));
}
// 11. This method will round x to the nearest hundredthPlace
// roundToHundredthPlace(12.9756) =======&gt; 12.98
public static double roundToHundredthPlace(double x){
return 0.01 * Math.round(100*x);
}
public static void main(String[] args) {
System.out.println("1. DISTANCE: " + distance(-5, 8));
System.out.println("2. MAX: " + maximum(-5, 8));
System.out.println("3. MAX: " + maximum(-5, -8, -2));
System.out.println("4. Random HS Class: " + getRandomClass());
System.out.println("5. Sphere Surface Area: " + sphereSurfaceArea(9)+ " sq. units");

System.out.println("6. Sphere Volume: " + sphereVolume(2)

+ " cubic units");

System.out.println("7. Hypotenuse: " + hypotenuse(7, 9));

System.out.println("8. Segment Length: "
+ segmentLength(new Point(1, 3), new Point(-2, 7)));

System.out.println("9. Smallest Acute Angle: "
+ getSmallestAngleOfRightTri(5, 7));

System.out.println("10. Round To Hundred Place: "
+ roundToHundredPlace(1297));

System.out.println("11. Round To Hundredth Place: "
+ roundToHundredthPlace(12.9756));

}
}