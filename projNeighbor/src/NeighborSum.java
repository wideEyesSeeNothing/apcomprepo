
import java.util.*;
import java.io.*;
import java.util.stream.IntStream;

public class NeighborSum {

	private int[][] chart = { 
			{ 1, 2, 3, 4, 5 }, 
			{ 6, 7, 8, 9, 0 }, 
			{ 6, 7, 1, 2, 5 }, 
			{ 6, 7, 8, 9, 0 },
			{ 5, 4, 3, 2, 1 }, 
			{ 0, 0, 0, 0, 0 } };
	private int[][] bigChart = new int[chart.length+1][chart[0].length+1];
	private int sum(int row, int col) {
		return Arrays.stream(bigChart).skip(row).limit(3).map(x -> Arrays.copyOfRange(x,col,col+3)).flatMapToInt(IntStream::of).sum() - chart[row][col];
	}


	public void go() throws Exception {
		for(int i = 0; i < chart.length; i++)for(int j = 0; j < chart[0].length; j++)bigChart[i+1][j+1] = chart[i][j];
		Scanner file = new Scanner(new File("NeighborSumChart.dat"));
		int pairs = file.nextInt();
		for (int i = 0; i < pairs; i++) {
			int r = file.nextInt();
			int c = file.nextInt();
			System.out.println("The sum of " + r + "," + c + " is " + sum(r, c));
		}
	}

	public static void main(String[] args) throws Exception {
		NeighborSum x = new NeighborSum();
		x.go();
	}
}
