import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Write a description of class test here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class Magic {




    // no instance variables or constructors needed
    // all methods are static


    /**  Precondition:  square is an initialized matrix, MAX rows x MAX columns
     0 <= row < MAX
     Postcondition: returns the sum of the values in row
     */
    public static boolean sumRow (int[][] square){
        return Arrays.asList(square).parallelStream().mapToInt(x-> IntStream.of(x).sum()).distinct().count() == 1;
    }
    public static boolean sumCol(int[][] square){
        return Arrays.stream(Arrays.stream(square).reduce(new int[square[0].length], (a,b) -> {for (int i = 0; i < square[0].length; i++) {a[i] += b[i];}return a;})).distinct().count() == 1;
    }
    public static boolean sumDiag(int[][] square){
        int i = 0, sum1=0, sum2=0;
        for (;i<square[0].length;i++)
            sum1 += square[i][i];
        for (int j = 0; j < square[0].length; j++) {
            sum2 += square[i-1][j];
            i--;
        }
        return sum1 == sum2;
    }



    /**Precondition: square is initialized with integers.
     Action: Inspects every value in square, checking that each one is
     a unique integer ranging from 1..MAX*MAX
     Postcondition: Returns true if each value is unique from 1..MAX*MAX,
     otherwise returns false
     */
    static boolean unique(int[][] square){
        List<Integer> r = IntStream.range(1,16).boxed().collect(Collectors.toList());
        for (int[] j: square){
            for(int i: j){
                if(r.contains(i))
                    r.remove(Integer.valueOf(i));
            }
        }
        return r.size() == 0;
    }





    /** Precondition: square is initialized with integers.
     Action: Checks that row, col, and diagonal sums are equal and elements are unique
     Postcondition: Returns true if magic else return false.
     */
    public static boolean testMagic(int[][] square){
        return unique(square) && sumCol(square) && sumDiag(square) && sumRow(square);
    }

    public static void printTable(int[][] table){
        for(int[] i: table){
            System.out.println(Arrays.toString(i));
        }
    }


//Add methods to sumCol, SumDiag, and print the arrays in matrix format. 

    public static void main(String[] args) {

        int[][] one ={{13, 3, 2, 16},
                {8, 10, 11, 5},
                {12, 6, 7, 9},
                {1, 15, 14, 4}};

        int[][] two ={{1, 14, 8, 11},
                {15, 4, 10, 5},
                {12, 7, 13, 2},
                {6, 9, 3, 16}};

        int[][] three ={{8, 11, 14, 1},
                {13, 2, 7, 12},
                {3, 16, 9, 6},
                {10, 5, 4, 15}};

        int[][] four ={{16, 2, 5, 11},
                {3, 4, 10, 8},
                {9, 7, 3, 14},
                {6, 12, 15, 1}};

        int[][] five ={{4, 9, 15, 16},
                {15, 6, 10, 3},
                {14, 7, 11, 2},
                {1, 12, 8, 13}};

        int[][] six ={{1, 2, 3, 4},
                {2, 3, 4, 1},
                {3, 4, 1, 2},
                {4, 1, 2, 3}};



        printTable(one);
        System.out.println("MAGIC SQUARE? " + testMagic(one));
        System.out.println();
        printTable(two);
        System.out.println("MAGIC SQUARE? " + testMagic(two));
        System.out.println();
        printTable(three);
        System.out.println("MAGIC SQUARE? " + testMagic(three));
        System.out.println();
        printTable(four);
        System.out.println("MAGIC SQUARE? " + testMagic(four));
        System.out.println();

        printTable(five);
        System.out.println("MAGIC SQUARE? " + testMagic(five));
        System.out.println();
        printTable(six);
        System.out.println("MAGIC SQUARE? " + testMagic(six));

    }


}