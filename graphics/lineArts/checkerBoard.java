package lineArts;

import java.awt.*;
import javax.swing.*;
import java.awt.geom.*;

public class checkerBoard extends JFrame{
    public checkerBoard(){
        super("Checker Board");
        setSize(600,600);
        setVisible(true);
		repaint();
    }
    public void paint(Graphics g){
        super.paint(g);
        Graphics2D g2d = (Graphics2D) g;
        int iter =0;
        for (int i = 0; i < 600; i+=75) {
            iter++;
            for (int j = 0; j < 600; j+=75) {
                g2d.setColor((iter % 2 ==0)?(Color.red):(Color.black));
                g2d.fillRect(i,j,75,75);
                try {
                    Thread.currentThread().sleep(100);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                iter++;
            }
        }
    }
    public static void main(String args[]){
        checkerBoard app = new checkerBoard();
        app.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}