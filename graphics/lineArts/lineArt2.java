package lineArts;

import java.awt.*;
import javax.swing.*;
import java.awt.geom.*;

public class lineArt2 extends JFrame{
    public lineArt2(){
        super("Line Art 2");
        setSize(600,600);
        setVisible(true);
		repaint();
    }
    public void paint(Graphics g){
        super.paint(g);
        Graphics2D g2d = (Graphics2D) g;
        for (int i = 0; i <=200; i+=10) {
            g2d.setColor(Color.red);
            g2d.draw(new Line2D.Double(500,300-i,510-i,100));
            g2d.setColor(Color.blue);  
            g2d.draw(new Line2D.Double(500,300+i,490-i,500));
            g2d.setColor(Color.green);  
            g2d.draw(new Line2D.Double(100,300-i,110+i,100));
            g2d.setColor(Color.magenta);  
            g2d.draw(new Line2D.Double(100,300+i,110+i,500));
        }
    }
    public static void main(String args[]){
        lineArt2 app = new lineArt2();
        app.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}