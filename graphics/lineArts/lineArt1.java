package lineArts;

import java.awt.*;
import javax.swing.*;
import java.awt.geom.*;

public class lineArt1 extends JFrame{
    public lineArt1(){
        super("Line Art 1");
        setSize(600,600);
        setVisible(true);
		repaint();
    }
    public void paint(Graphics g){
        super.paint(g);
        Graphics2D g2d = (Graphics2D) g;
        
        for (int i = 0; i <=250; i+=50) {
            g2d.setColor(Color.red);
            g2d.draw(new Line2D.Double(300,0+i,350+i,300));
            g2d.setColor(Color.blue);
            g2d.draw(new Line2D.Double(300,600-i,350+i,300));
            g2d.setColor(Color.green);         
            g2d.draw(new Line2D.Double(300,0+i,250-i,300));
            g2d.setColor(Color.magenta);
            g2d.draw(new Line2D.Double(300,600-i,250-i,300));
        }
    }
    public static void main(String args[]){
        lineArt1 app = new lineArt1();
        app.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}