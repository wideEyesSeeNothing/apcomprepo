import java.util.Random;
import java.awt.*;
import javax.swing.*;

public class DrawingCircles extends JFrame {
	public DrawingCircles() {
		super("Drawing Circles");
		setSize(800, 800);
		setVisible(true);
		repaint();
	}

	public void paint(Graphics g) {

		super.paint(g);
		Graphics2D g2 = (Graphics2D) g;
		g2.setColor(Color.black);
		g2.fillRect(0,0,800,800);
		g2.setColor(Color.white);
		for (int i = 0; i < 800; i+=50) {
			g2.fillRect(i,0,10,800);
			g2.fillRect(0,i,800,10);
			try {
				Thread.currentThread().sleep(200);
			} catch (Exception e) {
				//TODO: handle exception
				e.printStackTrace();
			}
		}

	} //end of public void paint

	public static void main(String[] args) 
{

  DrawingCircles application = new DrawingCircles();
  application.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
}

}
