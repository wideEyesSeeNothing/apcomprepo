    import java.util.Random;
import java.awt.*;
import javax.swing.*;

public class RealKewl extends JFrame {
	public RealKewl() {
		super("crosses in crosses");
		setSize(800, 800);
		setVisible(true);
		repaint();
	}

	public void paint(Graphics g) {

		super.paint(g);
		Graphics2D g2 = (Graphics2D) g;
		g2.setColor(Color.black);
		g2.fillRect(0,0,800,800);
		try {
			Thread.currentThread().sleep(400);
		} catch (Exception e) {
			//TODO: handle exception
			e.printStackTrace();
		}
		g2.setColor(Color.white);
		for (int i = 1; i < 7; i++) {
			int iter = (int)Math.pow(2,i);
			for (int j = 0; j < 1600; j += (1600/iter)){
				g2.fillRect(j/2,0,5,800);
				try {
					Thread.currentThread().sleep(100);
				} catch (Exception e) {
					//TODO: handle exception
					e.printStackTrace();
				}
				g2.fillRect(0,800-(j/2),800,5);
				try {
					Thread.currentThread().sleep(100);
				} catch (Exception e) {
					//TODO: handle exception
					e.printStackTrace();
				}
			}
			
		}

	} //end of public void paint

	public static void main(String[] args) 
{

  RealKewl application = new RealKewl();
  application.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
}

}

